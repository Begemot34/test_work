package pw.kozhanov.qrcheckscanner;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import pw.kozhanov.qrcheckscanner.models.Check;
import pw.kozhanov.qrcheckscanner.models.CheckDetails;
import pw.kozhanov.qrcheckscanner.models.CheckWithDetails;
import pw.kozhanov.qrcheckscanner.models.ScannedCheck;

public interface ChecksManager {
    void CheckProcessed(ScannedCheck scannedCheck, @Nullable CheckDetails checkDetails);
    LiveData<List<CheckWithDetails>> getChecks();
}
