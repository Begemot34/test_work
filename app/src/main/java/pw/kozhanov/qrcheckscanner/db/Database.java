package pw.kozhanov.qrcheckscanner.db;

import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import pw.kozhanov.qrcheckscanner.models.CheckDetails;
import pw.kozhanov.qrcheckscanner.models.CheckWithDetails;
import pw.kozhanov.qrcheckscanner.models.ScannedCheck;

@androidx.room.Database(entities = {ScannedCheck.class, CheckDetails.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class Database extends RoomDatabase {
    public abstract DatabaseDao getDatabaseDao();
}
