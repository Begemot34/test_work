package pw.kozhanov.qrcheckscanner.db;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import pw.kozhanov.qrcheckscanner.models.CheckDetails;
import pw.kozhanov.qrcheckscanner.models.CheckWithDetails;
import pw.kozhanov.qrcheckscanner.models.ScannedCheck;

@Dao
public interface DatabaseDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ScannedCheck scannedCheck);
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(CheckDetails checkDetails);

    @Query("SELECT * FROM scannedcheck LEFT JOIN checkdetails ON scannedcheck.fiscalNumber = checkdetails.fiscalDocumentNumber")
    LiveData<List<CheckWithDetails>>getDetailedCheck();
}
