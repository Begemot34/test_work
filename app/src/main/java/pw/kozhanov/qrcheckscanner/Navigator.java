package pw.kozhanov.qrcheckscanner;

import android.util.Log;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import pw.kozhanov.qrcheckscanner.models.CheckWithDetails;
import pw.kozhanov.qrcheckscanner.ui.CheckListFragment;
import pw.kozhanov.qrcheckscanner.ui.DetailsFragment;
import pw.kozhanov.qrcheckscanner.ui.ScanerFragment;

public class Navigator {
    private final FragmentManager fragmentManager;
    private static final String TAG = "Navigator";

    public Navigator(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    public void showScanner(){
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container,new ScanerFragment());
        fragmentTransaction.addToBackStack("scanner");
        fragmentTransaction.commit();
    }

    public void showCheckList(){
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, new CheckListFragment());
        fragmentTransaction.commit();
    }

    public void showDetails(CheckWithDetails checkWithDetails){
        currSelectedDetails=checkWithDetails;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container,new DetailsFragment());
        fragmentTransaction.addToBackStack("checkDetails");
        fragmentTransaction.commit();
    }

    public void back(){
        if(fragmentManager.getBackStackEntryCount()>0) {
            fragmentManager.popBackStack();
        }
    }

    private CheckWithDetails currSelectedDetails;

    public CheckWithDetails getCurrSelectedDetails() {
        return currSelectedDetails;
    }
}
