package pw.kozhanov.qrcheckscanner.ui;


import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import pw.kozhanov.qrcheckscanner.R;
import pw.kozhanov.qrcheckscanner.databinding.FragmentCheckListBinding;
import pw.kozhanov.qrcheckscanner.models.CheckWithDetails;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class CheckListFragment extends Fragment implements EasyPermissions.PermissionCallbacks{


    private static final int RC_CAMERA = 42;

    public CheckListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentCheckListBinding binding = FragmentCheckListBinding.inflate(inflater);
        init(binding);
        return binding.getRoot();
    }

    FragmentCheckListBinding binding;
    private void init(FragmentCheckListBinding binding) {

        this.binding=binding;
        final ChecksAdapter checksAdapter = new ChecksAdapter(new ChecksAdapter.ItemClick() {
            @Override
            public void itemClicked(CheckWithDetails checkWithDetails) {
                ((MainActivity)getActivity()).getNavigator().showDetails(checkWithDetails);
            }
        });
        ((MainActivity)getActivity()).getChecksManager().getChecks().observe(this, new Observer<List<CheckWithDetails>>() {
            @Override
            public void onChanged(List<CheckWithDetails> checkWithDetails) {
                checksAdapter.setData(checkWithDetails);
            }
        });
        binding.rvChecks.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.rvChecks.setAdapter(checksAdapter);
        binding.floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).getNavigator().showScanner();
            }
        });
        requestPermission();
    }

    @AfterPermissionGranted(RC_CAMERA)
    private void requestPermission() {
        String[] perms = {Manifest.permission.CAMERA};
        if (EasyPermissions.hasPermissions(getContext(), perms)) {
            binding.floatingActionButton.show();
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, "дайте разрешение для сканирования",
                    RC_CAMERA, perms);
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        binding.floatingActionButton.show();

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        binding.floatingActionButton.hide();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }
}
