package pw.kozhanov.qrcheckscanner.ui;

import androidx.appcompat.app.AppCompatActivity;
import pw.kozhanov.qrcheckscanner.ChecksManager;
import pw.kozhanov.qrcheckscanner.Navigator;
import pw.kozhanov.qrcheckscanner.QRChecksManager;
import pw.kozhanov.qrcheckscanner.R;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    private Navigator navigator;
    private ChecksManager checksManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        navigator = new Navigator(getSupportFragmentManager());
        if(savedInstanceState==null) {
            navigator.showCheckList();
        }
        checksManager = new QRChecksManager(getApplicationContext());
    }

    public Navigator getNavigator() {
        return navigator;
    }

    public ChecksManager getChecksManager() {
        return checksManager;
    }

    @Override
    public void onBackPressed(){
        getNavigator().back();
    }
}
