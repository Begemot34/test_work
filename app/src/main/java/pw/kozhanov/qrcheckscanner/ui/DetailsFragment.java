package pw.kozhanov.qrcheckscanner.ui;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import pw.kozhanov.qrcheckscanner.R;
import pw.kozhanov.qrcheckscanner.databinding.FragmentDetailsBinding;
import pw.kozhanov.qrcheckscanner.models.CheckDetails;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class DetailsFragment extends Fragment {
    private static final String TAG = "DetailsFragment";

    public DetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentDetailsBinding binding = FragmentDetailsBinding.inflate(inflater);
        init(binding);
        return binding.getRoot();
    }

    private void init(FragmentDetailsBinding binding) {
        final CheckDetails checkDetails = ((MainActivity) getActivity()).getNavigator().getCurrSelectedDetails().getCheckDetails();
        if(checkDetails!=null) {
            binding.tvContent.setText(Html.fromHtml(checkDetails.getDetails()));
        }
    }

}
