package pw.kozhanov.qrcheckscanner.ui;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import pw.kozhanov.qrcheckscanner.databinding.CheckItemBinding;
import pw.kozhanov.qrcheckscanner.models.CheckWithDetails;

public class ChecksAdapter extends RecyclerView.Adapter<ChecksAdapter.CheckItemHolder> {
    private static final String TAG = "ChecksAdapter";
    private List<CheckWithDetails>checkWithDetails=new ArrayList<>();
    private final ItemClick itemClick;

    public ChecksAdapter(ItemClick itemClick) {
        this.itemClick = itemClick;
    }

    interface ItemClick{
        void itemClicked(CheckWithDetails checkWithDetails);
    }

    public void setData(List<CheckWithDetails>checkWithDetails){
        if(checkWithDetails==null){
            return;
        }
        this.checkWithDetails=checkWithDetails;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public CheckItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        CheckItemBinding binding = CheckItemBinding.inflate(inflater, parent, false);
        return new CheckItemHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull CheckItemHolder holder, int position) {
        holder.binding.setCheck(checkWithDetails.get(position));
    }

    @Override
    public int getItemCount() {
        return checkWithDetails.size();
    }



    class CheckItemHolder extends RecyclerView.ViewHolder{
        CheckItemBinding binding;
        public CheckItemHolder(@NonNull View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClick.itemClicked(checkWithDetails.get(getAdapterPosition()));
                }
            });
        }
    }
}
