package pw.kozhanov.qrcheckscanner.ui;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.DecodeCallback;
import com.google.common.base.Splitter;
import com.google.zxing.Result;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import pw.kozhanov.qrcheckscanner.api.NetworkService;
import pw.kozhanov.qrcheckscanner.databinding.FragmentScanerBinding;
import pw.kozhanov.qrcheckscanner.models.CheckDetails;
import pw.kozhanov.qrcheckscanner.models.CheckWithDetails;
import pw.kozhanov.qrcheckscanner.models.DummyPage;
import pw.kozhanov.qrcheckscanner.models.ScannedCheck;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ScanerFragment extends Fragment {
    private static final String TAG = "ScanerFragment";
    private CodeScanner mCodeScanner;

    public ScanerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentScanerBinding binding = FragmentScanerBinding.inflate(inflater);
        init(binding);
        return binding.getRoot();
    }

    private void init(final FragmentScanerBinding binding) {
        final Activity activity = getActivity();
        mCodeScanner = new CodeScanner(activity, binding.scannerView);
        mCodeScanner.setFormats(CodeScanner.TWO_DIMENSIONAL_FORMATS);
        mCodeScanner.startPreview();
        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                //ui thread just for showing toast
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, result.getText(), Toast.LENGTH_SHORT).show();
                        binding.syncLayout.setVisibility(View.VISIBLE);
                        final ScannedCheck scannedCheck = parseCheck(result);
                        if(scannedCheck==null){
                            Toast.makeText(activity, "чек не распознан", Toast.LENGTH_SHORT).show();
                            binding.syncLayout.setVisibility(View.GONE);
                            mCodeScanner.startPreview();
                            binding.syncLayout.setVisibility(View.GONE);
                            return;
                        }
                        NetworkService.getInstance().getFocsyApi().getCheckDetails(scannedCheck.getFiscalNumber(), scannedCheck.getFiscalSign()
                                , scannedCheck.getFiscalDocument(), scannedCheck.getDateOfTransaction(), scannedCheck.getSum()).enqueue(new Callback<DummyPage>() {
                            @Override
                            public void onResponse(@NonNull Call<DummyPage> call, @NonNull Response<DummyPage> response) {
                                if (response.isSuccessful()) {
                                    if(response.body()!=null) {
                                        Log.d(TAG, "getCheckDetails: success");
                                        ((MainActivity) activity).getChecksManager()
                                                .CheckProcessed(scannedCheck, new CheckDetails(scannedCheck.getFiscalNumber(), response.body().getContent()));
                                    }
                                }else {
                                    Log.d(TAG, "onResponse: "+call.request().toString());
                                    Log.d(TAG, "getCheckDetails: not take because "+response.code());
                                    ((MainActivity) activity).getChecksManager().CheckProcessed(scannedCheck,null);
                                }
                                ((MainActivity) activity).getNavigator().showCheckList();
                            }

                            @Override
                            public void onFailure(@NonNull Call<DummyPage> call, @NonNull Throwable t) {
                                Log.e(TAG, "onFailure: ", t);
                                Toast.makeText(getContext(),"ошибка обращения к API",Toast.LENGTH_LONG).show();
                                ((MainActivity) activity).getChecksManager().CheckProcessed(scannedCheck,null);
                                ((MainActivity) activity).getNavigator().showCheckList();
                            }
                        });
                    }
                });
            }
        });
    }

    private ScannedCheck parseCheck(Result result) {
        Map<String, String> mappedResult = Splitter.on("&").withKeyValueSeparator("=").split(result.getText());
        Log.d(TAG, "parseCheck: date is "+mappedResult.get("t"));
        String dateOfOrder = parceDate(mappedResult);
        String fiscalNumber = mappedResult.get("fn");
        String fiscalSign = mappedResult.get("fp");
        String sum = mappedResult.get("s");
        String fiscalDocument = mappedResult.get("i");
        Log.d(TAG, "parseCheck: "+dateOfOrder+", "+fiscalNumber+", "+fiscalSign+", "+sum+", "+fiscalDocument);
        if (dateOfOrder == null ||
                fiscalNumber == null ||
                fiscalSign == null ||
                sum == null ||
                fiscalDocument == null) {
            return null;
        } else {
            float floatSum = Float.parseFloat(sum)*100;
            int sumCap=(int) floatSum;
            return new ScannedCheck(Long.parseLong(fiscalNumber), Long.parseLong(fiscalSign),
                    Integer.parseInt(fiscalDocument), dateOfOrder, new Date(), sumCap);
        }
    }

    @SuppressLint("SimpleDateFormat")
    @Nullable
    private String parceDate(Map<String, String> mappedResult) {
        DateFormat df = new SimpleDateFormat("yyyyMMddhhmm", Locale.ENGLISH);
        String dateString = mappedResult.get("t");

        if (dateString == null) return null;
        //not need seconds
        dateString = dateString.substring(0,12);
        String dateWithoutT = dateString.replace("T", "");
        try {
            Date date = df.parse(dateWithoutT);
            DateFormat f = new SimpleDateFormat("dd.MM.yyyy HH:mm");
            return f.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCodeScanner.releaseResources();
    }
}
