package pw.kozhanov.qrcheckscanner.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class CheckDetails {
    @PrimaryKey
    private final long fiscalDocumentNumber;
    private final String details;

    public CheckDetails(long fiscalDocumentNumber, String details) {
        this.fiscalDocumentNumber = fiscalDocumentNumber;
        this.details = details;
    }

    public String getDetails() {
        return details;
    }

    public long getFiscalDocumentNumber() {
        return fiscalDocumentNumber;
    }

    @Override
    public String toString() {
        return "CheckDetails{" +
                "fiscalDocumentNumber=" + fiscalDocumentNumber +
                ", detailsSize='" + details.length() + '\'' +
                '}';
    }
}
