package pw.kozhanov.qrcheckscanner.models;

public class DummyPage {
    private final String content;

    public DummyPage(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}
