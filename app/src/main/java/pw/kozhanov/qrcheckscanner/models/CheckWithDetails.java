package pw.kozhanov.qrcheckscanner.models;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;

public class CheckWithDetails {
    @Embedded ScannedCheck scannedCheck;
    //@Embedded CheckDetails checkDetails;
    //@Relation(parentColumn = "fiscalDocument", entityColumn = "fiscalDocumentNumber") @Nullable public CheckDetails checkDetails;
    @Embedded CheckDetails checkDetails;

    public CheckWithDetails(ScannedCheck scannedCheck, @Nullable CheckDetails checkDetails) {
        this.scannedCheck = scannedCheck;
        this.checkDetails = checkDetails;
    }

    public ScannedCheck getScannedCheck() {
        return scannedCheck;
    }

    @Nullable
    public CheckDetails getCheckDetails() {
        return checkDetails;
    }

    @NonNull
    @Override
    public String toString() {
        return "CheckWithDetails{" +
                "scannedCheck=" + scannedCheck +
                ", checkDetails=" + checkDetails +
                '}';
    }
}
