package pw.kozhanov.qrcheckscanner.models;

import java.util.Date;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class ScannedCheck {
    @PrimaryKey
    private int fiscalDocument;
    private long fiscalNumber;
    private long fiscalSign;
    private String dateOfTransaction;
    private Date dateOfScan;
    private int sum;

    public void setFiscalDocument(int fiscalDocument) {
        this.fiscalDocument = fiscalDocument;
    }

    public void setFiscalNumber(long fiscalNumber) {
        this.fiscalNumber = fiscalNumber;
    }

    public void setFiscalSign(long fiscalSign) {
        this.fiscalSign = fiscalSign;
    }

    public void setDateOfTransaction(String dateOfTransaction) {
        this.dateOfTransaction = dateOfTransaction;
    }

    public void setDateOfScan(Date dateOfScan) {
        this.dateOfScan = dateOfScan;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public ScannedCheck(long fiscalNumber, long fiscalSign, int fiscalDocument,
                        String dateOfTransaction, Date dateOfScan, int sum) {
        this.fiscalNumber = fiscalNumber;
        this.fiscalSign = fiscalSign;
        this.fiscalDocument = fiscalDocument;
        this.dateOfTransaction = dateOfTransaction;
        this.dateOfScan = dateOfScan;
        this.sum = sum;
    }

    public long getFiscalNumber() {
        return fiscalNumber;
    }

    public long getFiscalSign() {
        return fiscalSign;
    }

    public int getFiscalDocument() {
        return fiscalDocument;
    }

    public String getDateOfTransaction() {
        return dateOfTransaction;
    }

    public Date getDateOfScan() {
        return dateOfScan;
    }

    @Ignore
    public String getVMDatas(){
        return "сырые данные сканирования: "+toString();
    }

    public String getVMDateOfScan(){
        return "Дата сканирования "+dateOfScan.toString();
    }

    public int getSum() {
        return sum;
    }

    @Override
    public String toString() {
        return "ScannedCheck{" +
                "fiscalDocument=" + fiscalDocument +
                ", fiscalNumber=" + fiscalNumber +
                ", fiscalSign=" + fiscalSign +
                ", dateOfTransaction='" + dateOfTransaction + '\'' +
                ", dateOfScan=" + dateOfScan +
                ", sum=" + sum +
                '}';
    }
}
