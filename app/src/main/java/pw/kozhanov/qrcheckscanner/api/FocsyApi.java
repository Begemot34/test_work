package pw.kozhanov.qrcheckscanner.api;

import pw.kozhanov.qrcheckscanner.models.DummyPage;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface FocsyApi {
    @GET("/offline/check")
    public Call<DummyPage> getCheckDetails(@Query("fiscalNumber")long fiscalNumber,
                                           @Query("fiscalSign") long fiscalSign, @Query("fiscalDocument")int fiscalDocument,
                                           @Query("date")String date, @Query("sum")int sum);
}
