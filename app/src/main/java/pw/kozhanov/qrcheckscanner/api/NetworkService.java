package pw.kozhanov.qrcheckscanner.api;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import okhttp3.ResponseBody;
import pw.kozhanov.qrcheckscanner.models.DummyPage;
import retrofit2.Converter;
import retrofit2.Retrofit;

public class NetworkService {
    private static NetworkService mInstance;
    private Retrofit retrofit;

    private NetworkService(){
        retrofit = new Retrofit.Builder()
                .baseUrl("https://focsy.network")
                .addConverterFactory(PageAdapter.FACTORY)
                .build();
    }

    public static NetworkService getInstance() {
        if (mInstance == null) {
            mInstance = new NetworkService();
        }
        return mInstance;
    }

    public FocsyApi getFocsyApi(){
        return retrofit.create(FocsyApi.class);
    }

    static final class PageAdapter implements Converter<ResponseBody, DummyPage> {
        static final Converter.Factory FACTORY = new Converter.Factory() {
            @Override
            public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
                if (type == DummyPage.class) return new PageAdapter();
                return null;
            }
        };

        @Override
        public DummyPage convert(ResponseBody responseBody) throws IOException {
            Document document = Jsoup.parseBodyFragment(responseBody.string());
            Element value = document.body();
            String content = value.html();
            return new DummyPage(content);
        }
    }

}