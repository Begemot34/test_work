package pw.kozhanov.qrcheckscanner;

import android.content.Context;
import android.util.Log;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.room.Room;
import pw.kozhanov.qrcheckscanner.db.Database;
import pw.kozhanov.qrcheckscanner.db.DatabaseDao;
import pw.kozhanov.qrcheckscanner.models.Check;
import pw.kozhanov.qrcheckscanner.models.CheckDetails;
import pw.kozhanov.qrcheckscanner.models.CheckWithDetails;
import pw.kozhanov.qrcheckscanner.models.ScannedCheck;

public class QRChecksManager implements ChecksManager {
    private static final String TAG = "QRChecksManager";
    private final Database database;

    public QRChecksManager(Context context) {
        database = Room.databaseBuilder(context,Database.class,"user")
                .allowMainThreadQueries()
                .build();
    }

/*    @Override
    public void CheckProcessed(CheckWithDetails check) {
        Log.d(TAG, "CheckProcessed: "+check.toString());
        database.getDatabaseDao().insert(check);
    }*/

    @Override
    public void CheckProcessed(ScannedCheck scannedCheck, @Nullable CheckDetails checkDetails) {
        Log.d(TAG, "CheckProcessed: "+scannedCheck.toString());
        database.getDatabaseDao().insert(scannedCheck);
        if(checkDetails!=null) {
            Log.d(TAG, "CheckProcessed: "+checkDetails.toString());
            database.getDatabaseDao().insert(checkDetails);
        }
    }

    @Override
    public LiveData<List<CheckWithDetails>> getChecks() {
        return database.getDatabaseDao().getDetailedCheck();
    }
}
